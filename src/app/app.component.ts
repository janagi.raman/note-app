import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Note-App';


  title1:string;
  body:string;

  notesList : noteObj[] = [];

  onSummit(){
    var obj = new noteObj();
    obj.title = this.title1;
    obj.body = this.body;

    this.notesList.push(obj);
  }
}

export class noteObj {
  title : string;
  body : string;
}