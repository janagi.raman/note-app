import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DlsComponent } from './dls.component';

describe('DlsComponent', () => {
  let component: DlsComponent;
  let fixture: ComponentFixture<DlsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DlsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
