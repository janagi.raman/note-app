import { Component, Input, OnInit } from '@angular/core';
import { noteObj } from '../app.component';

@Component({
  selector: 'app-dls',
  templateUrl: './dls.component.html',
  styleUrls: ['./dls.component.css']
})
export class DlsComponent implements OnInit {

@Input() chilData: noteObj[];

  constructor() { }

  ngOnInit(): void {
  }

}
